class HomeController < ApplicationController
  def index
    if session[:user_id]
      @user = User.find_by_id(session[:user_id])
    end

    unless @user
      @user = User.create!
      @user.name = "Guest#{@user.id}"
      @user.save!
      session[:user_id] = @user.id
    end
    message_id = MessageBus.publish "/channel", {text: "New connection: #{@user.name}",}
    # render json: {hi: 7}
  end
end
