class ChatController < ApplicationController
  before_action :ready_user

  def speak
    message_id = MessageBus.publish "/channel", {text: params[:text], name: @user.name}
    head 204
  end

  def change_name
    if params[:name].present? && !params[:name].downcase.starts_with?('guest')
      @user.update(name: params[:name])
      head 204
    else
      head 422
    end
  end

private
  def ready_user
    if session[:user_id]
      @user = User.find_by_id(session[:user_id])
    end

    raise unless @user
  end
end
