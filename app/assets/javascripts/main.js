$( document ).ready(function() {
    var name = $('#name').text();
    MessageBus.start(); // call once at startup

    // how often do you want the callback to fire in ms
    MessageBus.callbackInterval = 1000;
    MessageBus.enableLongPolling = false;
    //MessageBus.alwaysLongPoll = true;
    MessageBus.subscribe("/channel", function(data){
        // data shipped from server
        console.log(data);
        addLine(data.text, data.name);
    });

    function addLine(text, name){
        if(name){
            $('#chatLog').append('<div class="chat-line"><b>'+name+'</b>: '+text+'</div>');
        }else{
            $('#chatLog').append('<div class="chat-line"><i>'+text+'</i></div>');
        }
    }

    $('#chatInput').keyup(function(e){
        if(e.keyCode == 13) {
            var text = $('#chatInput').val();
            $('#chatInput').val('');
            //addLine(text, name);
            $.post("/speak", {text: text}, null, "json" );
        }
    });

    $('#name').click(function(){
        var nameNew = prompt('name?');
        var changeRequest = $.post("/change_name", {name: nameNew}, null, "json").done(function(){
            name = nameNew;
            $('#name').html(name);
        });
    })

});
